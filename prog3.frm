#-
S   a,b,c,d,e,f,g,h,i,j,k,l,m,n;
L   H = (4*a^4 + b + c + d + i^4 + g * n^3 + 1)^10 + (a*h + e + f*i*j + g + h)^8 + (i + j + k + l + m + n)^12;
Format O1;
.sort
ExtraSymbols,array,w;
Format C;
#optimize H
#write <out3.c> "std::array<double, `optimmaxvar_'> w;"
#write <out3.c> "%O"
#write <out3.c> "double H { %E };",H
.end
