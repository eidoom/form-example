# [form-example](https://gitlab.com/eidoom/form-example)

## Download repo
```shell
cd ~/git
git clone git@gitlab.com:eidoom/form-example.git
```

## Install [FORM](https://github.com/vermaseren/form)
With Fedora specific packages,
```shell
sudo dnf install git zlib gmp gcc gcc-c++ openmpi autoconf automake make
cd ~/git
git clone git@github.com:vermaseren/form.git
cd form
autoreconf -i
cd ~/local
mkdir form
cd ~/build
mkdir form
cd form
~/git/form/configure --prefix=$HOME/local/form
make -j
make install
echo "export PATH=$HOME/local/form/bin:$PATH" >> ~/.zshrc
source ~/.zshrc
```

## Run an example
```shell
cd ~/git/form-example
tform prog0.frm
```

## Examples

### Hello world

`prog0.frm` demonstrates simple usage.

### Operation optimisation
* This is discussed in the documentation [here](https://www.nikhef.nl/~form/maindir/documentation/reference/online/online.html#optimization).
* `prog1.frm` demonstrates different optimisation levels
* `prog2.frm` demonstrates formatting the results for use in C code and outputting this to file
    * Clean up output with
    ```shell
    sudo dnf install clang
    clang-format -i out*.c
    ```
* `prog3.frm` demonstrates outputting in C++ format
    * Could also apply float type templates
    ```shell
    sed -i "s/\([0-9]\+\)\ /static_cast<T>(\1.) /g" out3.c
    sed -i "s/\ \([0-9]\+\)/ static_cast<T>(\1.)/g" out3.c
    sed -i "s/\([0-9]\+\)\*/static_cast<T>(\1.)*/g" out3.c
    ```
    * Or C++ type powers
    ```shell
    sed -i "s/pow/std::pow/g" out3.c
    ```
* For simultaneously optimising multiple expressions, see [this script](https://gitlab.com/jetdynamics/njet-tools/-/blob/master/pentagon/math2njet/mmppp/conv-irrules-form.sh), which generates a FORM script to do this.
See also [Section 4.2](https://www.nikhef.nl/~form/maindir/publications/optim.pdf).

## Links

* [FORM GitHub repo](https://github.com/vermaseren/form)
* [FORM homepage](https://www.nikhef.nl/~form/maindir/maindir.html)
* [FORM documentation](https://www.nikhef.nl/~form/maindir/documentation/reference/online/online.html)
