#-
S   a,b,c,d,e,f,g,h,i,j,k,l,m,n;
L   G = (4*a^4+b+c+d + i^4 + g*n^3)^10 + (a*h + e + f*i*j + g + h)^8 + (i + j + k + l + m + n)^12;
L   H = G;
Format O1;
.sort
#optimize G
#write "Optimized with O1:"
#write "Optimized with Horner scheme: `optimscheme_'"
#write "Number of operations in output: `optimvalue_'"
#clearoptimize
.sort
Format O4,saIter=1000; * use 1000 iterations for optimization
#optimize H
#write "Optimized with O4:"
#write "Optimized with Horner scheme: `optimscheme_'"
#write "Number of operations in output: `optimvalue_'"
.end
